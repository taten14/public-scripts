DISPLAYNAME='Nicolas Ordoñez'
EMAIL='nicolaso@smartfran.com'
SUBSCRIPTION="Azure subscription 1"
USERPRINCIPALNAME=$(echo $EMAIL | tr '@' '_')#EXT#@mansillavictorhotmail.onmicrosoft.com
#DISPLAYNAME=$1
#PASSWORD=$(echo $RANDOM | md5sum | head -c 16; echo;)
PASSWORD=$(tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c 13  ; echo)
#EMAIL=$2
az account set --name "$SUBSCRIPTION"
az ad user create \
    --display-name "$DISPLAYNAME" \
    --password "$PASSWORD" \
    --mail $EMAIL \
    --user-principal-name $USERPRINCIPALNAME \
    --force-change-password-next-sign-in true


echo $PASSWORD