## Seteo de unidades

sudo mkdir /mnt/{svn,logs,sdb} && \
sudo sed -i 's/\/mnt/\/mnt\/sdb/g' /etc/fstab && \
sudo parted -s /dev/sdc mklabel gpt mkpart primary ext4 0% 100% && \
sudo mkfs -t ext4 /dev/sdc1 && \
sleep 10 && \
echo UUID=$(ls -lash /dev/disk/by-uuid/ | grep sdc1 | awk '{print $10}') /mnt/svn   auto    defaults,nofail,x-systemd.requires=cloud-init.service,comment=cloudconfig  0  2 | sudo tee -a /etc/fstab && \
sudo parted -s /dev/sdd mklabel gpt mkpart primary ext4 0% 100% && \
sudo mkfs -t ext4 /dev/sdd1 && \
sleep 10 && \
echo UUID=$(ls -lash /dev/disk/by-uuid/ | grep sdd1 | awk '{print $10}') /mnt/logs   auto    defaults,nofail,x-systemd.requires=cloud-init.service,comment=cloudconfig  0  2 | sudo tee -a /etc/fstab && \
sudo  chown -R www-data:www-data  /mnt/svn && \
sudo chmod -R 775 /mnt/svn

# Confriguracion de SSH en el puerto 51001

sudo sed -i 's\Port 22\Port 51001\g' /etc/ssh/sshd_config

# Instalacion de paquetes

sudo apt update && \
sudo apt install -y apache2 apache2-utils && \
sudo apt install -y vim tree subversion libsvn-dev libapache2-mod-svn subversion-tools && \
sudo a2enmod dav dav_svn && \
sudo systemctl restart apache2 && \

# Configuracion de SVN

sudo tee /etc/apache2/mods-enabled/dav_svn.conf<<EOF
Alias /svn /mnt/svn
<VirtualHost *:80>
     ServerName svn.example.com
    <Location /svn>
        DAV svn
        SVNParentPath /mnt/svn
        AuthType Basic
        AuthName "Subversion Repository"
        AuthUserFile /etc/svn-user
        Require valid-user
    </Location>
    ErrorLog /mnt/logs/svn-error.log
    CustomLog /mnt/logs/svn-access.log combined
</VirtualHost>
EOF

sudo apachectl -t && \
sudo systemctl restart apache2
