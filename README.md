# Public Scripts

## Description

Repositorio creado para almacenar scripts que seran llamados por frameworks de IaaS com por ejemplo Terraform

## Usage

Invocar al script mediante el endpoint raw para luego correrlo mediante el interperete deseado.

```bash
az vm run-command invoke \
    --command-id RunPowerShellScript \
    --name SFCG-SRDB-TEST01 \
    --resource-group SFCG-REGR-DEV \
    --scripts @installZabbixAgent2.ps1
```

