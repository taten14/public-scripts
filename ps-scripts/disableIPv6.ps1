
#Requires -Version 4.0
#Runas SYSTEM / admin
try{
    $params=@{
        AppName="Disable IPV6"
    }
    Disable-NetAdapterBinding -Name 'Ethernet' -ComponentID 'ms_tcpip6'
}
catch
{
    Write-Host "no se pudo instalar la app $(params.AppName)`r`n $_"
}