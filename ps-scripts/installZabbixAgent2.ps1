#Requires -Version 4.0
#Runas SYSTEM / admin

try
    {
    $params=@{
        Uri="https://cdn.zabbix.com/zabbix/binaries/stable/5.0/5.0.29/zabbix_agent2-5.0.29-windows-amd64-openssl.msi"
        OutFile="D:\zabbix_agent2.msi"
        ServerAddress=$args
        }
    $HOSTNAME=$env:COMPUTERNAME
    $ARGS='/l*v log.log /i '+$($params.OutFile)+' /qr SERVER="'+$($params.ServerAddress)+'" SERVERACTIVE="'+$($params.ServerAddress)+':10051" ENABLEPATH="1" HOSTNAME="'+$($env:COMPUTERNAME)+'" LISTENIP="0.0.0.0" ENABLEPERSISTENTBUFFER="1" PERSISTENTBUFFERPERIOD="15d" PERSISTENTBUFFERFILE="C:\Program Files\Zabbix Agent 2\persistentbuffer.zbx"'
    #Write-Host $ARGS
    Start-Process "msiexec.exe" -ArgumentList $ARGS
    }
catch
    {
    Write-Host "no se pudo crear el Event `r`n $_"
    }